import axios from 'axios';
import {createActions, handleActions} from 'redux-actions';
import {API_URL} from "../../config";

const rest = (accessToken) => axios.create({
  //Заменить на свой API_URL
  baseURL: `${API_URL}`,
  headers: {
    'Content-Type': 'application/json',
    'X-Auth-Token': accessToken
  },
});

export const { users } = createActions({
  USERS: () =>
    rest(localStorage.getItem('token'))
      .get('/users')
      .then((response) => {
          const res = response.data;
            console.log('response', res)
              if (res) {
              return res;
            }
      })
      .catch((error) => {
        return false;
      }),

});

export default handleActions(
  {
    [users]: (state, { payload }) => (
      {
        ...state,
        users: payload
      }
    ),
  },
  {},
);
