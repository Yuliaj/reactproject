import { createStore, applyMiddleware, compose } from 'redux';
import reduxPromise from 'redux-promise';
import thunk from 'redux-thunk';
import rootReducer from './modules';


// dev tools middleware
const devtool = window.__REDUX_DEVTOOLS_EXTENSION__;

const middlewares = [reduxPromise,thunk];
const reduxDevTools = (devtool && devtool()) || compose;

const defaultState = {
  users: null
}

// create a redux store with our reducer above and middleware
const store = createStore(
  rootReducer,
  defaultState,
  compose(
    applyMiddleware(...middlewares),
    reduxDevTools,
  ),
);

export default store;

