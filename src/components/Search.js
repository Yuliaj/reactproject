import useLocalStorage from "../hooks/useLocalStorage";
import React from "react";
import useLogger from "../hooks/useLogger";

export default function Search() {
    const [name, setName] = useLocalStorage("name", "")
    useLogger(name)
    return (
        <div>
            <h2>Enter some text</h2>
            <input type="text" value={name} onChange={e => setName(e.target.value)}/>
        </div>
);
}