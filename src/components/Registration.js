import '../App.css';
import React, {Component} from "react";
// import {API_URL} from "../Config";
import {API_URL} from "../config";
import {Button} from "react-bootstrap";
class Registration extends Component{
    constructor() {
        super();
        this.state={
            firstName: null,
            lastName: null,
            login: null,
            password: null,
            email: null
        }
    }
    async register(){
        const response = await fetch(`${API_URL}/auth/registration`, {
            headers: {
                'Accept': 'application/json; charset=utf-8',
                'Content-Type': 'application/json; charset=utf-8'
            },
            method: "POST",
            body: JSON.stringify(this.state)
        });
        const data = await response.json()
        console.warn("result", data)
        localStorage.setItem('token', data.jwtToken)
    }

    render(){
        return (
            <div>
                <h1>Registration page</h1>
                <form>
                <div className="form-group">
                    <label>First name</label>
                    <input type="text" className="form-control" placeholder="First name" onChange={(event) => {
                        this.setState({firstName: event.target.value})
                    }} />
                </div>

                <div className="form-group">
                    <label>Last name</label>
                    <input type="text" className="form-control" placeholder="Last name" onChange={(event) => {
                        this.setState({lastName: event.target.value})
                    }}/>
                </div>

                <div className="form-group">
                    <label>Email address</label>
                    <input type="email" className="form-control" placeholder="Enter email" onChange={(event) => {
                        this.setState({email: event.target.value})
                    }}/>
                </div>

                <div className="form-group">
                    <label>Password</label>
                    <input type="password" className="form-control" placeholder="Enter password" onChange={(event) => {
                        this.setState({password: event.target.value})
                    }}/>
                </div>

                <div className="form-group">
                    <label>Login</label>
                    <input type="text" className="form-control" placeholder="Enter login" onChange={(event) => {
                        this.setState({login: event.target.value})
                    }}/>
                </div>

                <button type="submit" className="btn btn-primary btn-block" onClick={() => this.register()}>Sign Up</button>
                <p className="forgot-password text-right">
                    Already registered <a href="/login">sign in?</a>
                </p>
            </form>
            </div>
        );
    }
}
export default Registration;