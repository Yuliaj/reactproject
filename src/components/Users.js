import '../App.css';
import React, { Component } from "react";
import {connect} from "react-redux";

import {users} from '../store/modules/users';
import {Button, ListGroupItem} from "react-bootstrap";


class Users extends Component {

  constructor(props) {
    super(props);
    this.state = { };
  }

  displayUsers = () => {
      this.props.users();
  }

  createUserList = () => {
      if(this.props.usersData!== null){
          return this.props.usersData.users.map((user) => {
              return (
                  <div >
                      <h1 color="red"><ListGroupItem key={user.id} >{user.firstName}  {user.lastName}</ListGroupItem></h1>
                      <h1>{ this.displayDetails(user)} </h1>
                  </div>
              )
          })
      } else {
          return ""
      }
  }

  displayDetails = (user) => {
      return (
          <div>
              <h2>{user.firstName}'s email: {user.email}</h2>
              <h2>{user.firstName}'s login: {user.login}</h2>
          </div>
      )
  }
  render() {
    const { usersData } = this.props;
    console.log('usersData', usersData)
    return (
      <div>
          <Button onClick={this.displayUsers}>Load users</Button>
          <h2 align="center">Users:</h2>
          <ul>
              {this.createUserList()}
          </ul>
          <hr/>
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  usersData: state.users,
});

const mapDispatchToProps = (dispatch) => ({
  users: () => dispatch(users()),
 });

export default connect(mapStateToProps, mapDispatchToProps)(Users);
