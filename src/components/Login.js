import '../App.css';
import React, {Component} from "react";
import {API_URL} from "../config";
import {Button} from "react-bootstrap";

class Login extends Component{
    constructor() {
        super();
        this.state={
            username: null,
            password: null
        }
    }
    async login(){
        const response = await fetch(`${API_URL}/auth/login`, {
            headers: {
                'Accept': 'application/json; charset=utf-8',
                'Content-Type': 'application/json; charset=utf-8'
            },
            method: "POST",
            body: JSON.stringify(this.state)
        });
        const data = await response.json();
        console.warn("result", data)
        localStorage.setItem('token', data.jwtToken)
        }

    render(){
        return (
            // <div>
            //     <h1>Login page</h1>
            //     <h2>Enter Login: <input type="text" onChange={(event) => {
            //         this.setState({username: event.target.value})
            //     }}/></h2>
            //     <h2>Enter Password: <input type="text" onChange={(event) => {
            //         this.setState({password: event.target.value})
            //     }}/></h2>
            //     <Button onClick={() => this.login() }>Login</Button>
            // </div>
            <form>
                <h3>Sign In</h3>

                <div className="form-group">
                    <label>Username</label>
                    <input type="username" className="form-control" placeholder="Enter username" onChange={(event) => {
                        this.setState({username: event.target.value})
                    }}/>
                </div>

                <div className="form-group">
                    <label>Password</label>
                    <input type="password" className="form-control" placeholder="Enter password" onChange={(event) => {
                        this.setState({password: event.target.value})
                    }}/>
                </div>
                <button type="submit" className="btn btn-primary btn-block" onClick={() => this.login()}>Submit</button>
                <p className="forgot-password text-right">
                    don't have <a href="/registration">an account </a>yet?
                </p>
            </form>
        );
    }

}
export default Login;