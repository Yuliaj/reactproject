import './App.css';
import React, {Component} from "react";
// import Navigation from "./components/Navigation";
import Login from "./components/Login";
import Registration from "./components/Registration";
import Home from "./components/Home";
import {BrowserRouter, Switch, Route} from 'react-router-dom';
import Search from "./components/Search";
import Users from "./components/Users";
import {Layout} from "./components/layout/Layout"
import {Navigation} from "./components/Navigation"

class App extends Component {
    render() {
        return (
            <React.Fragment>
                <Layout>
                    <BrowserRouter>
                        <Navigation/>
                            <Switch>
                                <Route path="/" exact component={Home}/>
                                <Route path="/login" component={Login}/>
                                <Route path="/registration" component={Registration}/>
                                <Route path="/search" component={Search}/>
                                <Route path="/getUsers" component={Users}/>
                            </Switch>
                    </BrowserRouter>
                </Layout>
            </React.Fragment>
        );
    }
}
export default App;